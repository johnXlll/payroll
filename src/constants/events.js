// Contains all of the events used in our global event bus
export const DEFAULT_EVENT = 'default_event'
export const CRUD = 'crud'
export const PAYROLL = 'payroll'
export const ABOUT = 'about'
export const ADD_NEW = 'addNew'
