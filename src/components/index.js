import About from './About'
import AddNew from './AddNew'
import BarChart from './Charts/BarChart'
import Dashboard from './Dashboard'
import Graph from './Graph'
import HelloWorld from './HelloWorld'
import PieChart from './Charts/PieChart'
import Printable from './Printable'
import Sample from './Sample'
import Slide from './Slide'

export default {
  About,
  AddNew,
  BarChart,
  Dashboard,
  Graph,
  HelloWorld,
  PieChart,
  Printable,
  Sample,
  Slide
}
